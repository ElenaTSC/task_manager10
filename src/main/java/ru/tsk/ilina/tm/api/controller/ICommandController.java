package ru.tsk.ilina.tm.api.controller;

public interface ICommandController {

    void showErrorCommand();

    void showErrorArgument();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showInfo();

    void exit();

}
