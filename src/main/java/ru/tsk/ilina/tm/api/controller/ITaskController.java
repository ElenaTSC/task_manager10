package ru.tsk.ilina.tm.api.controller;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

}
