package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    void clear();

    List<Project> findAll();

}
