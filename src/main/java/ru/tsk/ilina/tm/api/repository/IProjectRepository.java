package ru.tsk.ilina.tm.api.repository;

import ru.tsk.ilina.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    void clear();

    List<Project> findAll();

}
