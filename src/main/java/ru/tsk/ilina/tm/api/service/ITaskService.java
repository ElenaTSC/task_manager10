package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    void remove(Task task);

    void clear();

    List<Task> findAll();

}
