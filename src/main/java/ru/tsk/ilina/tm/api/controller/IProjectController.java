package ru.tsk.ilina.tm.api.controller;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

}
